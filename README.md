# ensemble learning base

#### 介绍
集成学习方法处理软件缺陷预测-基础

#### 算法
随机森林，极限树，梯度提升树，Adaboost，K近邻等  
('RF', 'ET', 'GDBT', 'Adaboost', 'KNN', 'Stacking')


#### 库与环境
sklearn，matplotlib，pandas，imblearn，pyecharts等  
运行环境：python 3.6及以上

#### 使用说明
运行stacking_data.py文件
最后结果生成于results.html，手动打开进行查看

#### 参与贡献
CSU SE 2019  
SE Master Team.
